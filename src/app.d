module app;

import std.stdio : writeln, readln;

void main()
{
    writeln("Version Tags {");

    version(DigitalMars) // DMD (Digital Mars D) is the compiler
        writeln("    DigitalMars");
    version(GNU) // GDC (GNU D Compiler) is the compiler
        writeln("    GNU");
    version(LDC) // LDC (LLVM D Compiler) is the compiler
        writeln("    LDC");
    version(SDC) // SDC (Stupid D Compiler) is the compiler
        writeln("    SDC");
    version(Windows) // Microsoft Windows systems
        writeln("    Windows");
    version(Win32) // Microsoft 32-bit Windows systems
        writeln("    Win32");
    version(Win64) // Microsoft 64-bit Windows systems
        writeln("    Win64");
    version(linux) // All Linux systems
        writeln("    linux");
    version(OSX) // macOS
        writeln("    OSX");
    version(iOS) // iOS
        writeln("    iOS");
    version(TVOS) // tvOS
        writeln("    TVOS");
    version(WatchOS) // watchOS
        writeln("    WatchOS");
    version(FreeBSD) // FreeBSD
        writeln("    FreeBSD");
    version(OpenBSD) // OpenBSD
        writeln("    OpenBSD");
    version(NetBSD) // NetBSD
        writeln("    NetBSD");
    version(DragonFlyBSD) // DragonFlyBSD
        writeln("    DragonFlyBSD");
    version(BSD) // All other BSDs
        writeln("    BSD");
    version(Solaris) // Solaris
        writeln("    Solaris");
    version(Posix) // All POSIX systems (includes Linux, FreeBSD, OS X, Solaris, etc.)
        writeln("    Posix");
    version(AIX) // IBM Advanced Interactive eXecutive OS
        writeln("    AIX");
    version(Haiku) // The Haiku operating system
        writeln("    Haiku");
    version(SkyOS) // The SkyOS operating system
        writeln("    SkyOS");
    version(SysV3) // System V Release 3
        writeln("    SysV3");
    version(SysV4) // System V Release 4
        writeln("    SysV4");
    version(Hurd) // GNU Hurd
        writeln("    Hurd");
    version(Android) // The Android platform
        writeln("    Android");
    version(Emscripten) // The Emscripten platform
        writeln("    Emscripten");
    version(PlayStation) // The PlayStation platform
        writeln("    PlayStation");
    version(PlayStation4) // The PlayStation 4 platform
        writeln("    PlayStation4");
    version(Cygwin) // The Cygwin environment
        writeln("    Cygwin");
    version(MinGW) // The MinGW environment
        writeln("    MinGW");
    version(FreeStanding) // An environment without an operating system (such as Bare-metal targets)
        writeln("    FreeStanding");
    version(CRuntime_Bionic) // Bionic C runtime
        writeln("    CRuntime_Bionic");
    version(CRuntime_DigitalMars) // DigitalMars C runtime
        writeln("    CRuntime_DigitalMars");
    version(CRuntime_Glibc) // Glibc C runtime
        writeln("    CRuntime_Glibc");
    version(CRuntime_Microsoft) // Microsoft C runtime
        writeln("    CRuntime_Microsoft");
    version(CRuntime_Musl) // musl C runtime
        writeln("    CRuntime_Musl");
    version(CRuntime_Newlib) // newlib C runtime
        writeln("    CRuntime_Newlib");
    version(CRuntime_UClibc) // uClibc C runtime
        writeln("    CRuntime_UClibc");
    version(CRuntime_WASI) // WASI C runtime
        writeln("    CRuntime_WASI");
    version(CppRuntime_Clang) // Clang Cpp runtime
        writeln("    CppRuntime_Clang");
    version(CppRuntime_DigitalMars) // DigitalMars Cpp runtime
        writeln("    CppRuntime_DigitalMars");
    version(CppRuntime_Gcc) // Gcc Cpp runtime
        writeln("    CppRuntime_Gcc");
    version(CppRuntime_Microsoft) // Microsoft Cpp runtime
        writeln("    CppRuntime_Microsoft");
    version(CppRuntime_Sun) // Sun Cpp runtime
        writeln("    CppRuntime_Sun");
    version(X86) // Intel and AMD 32-bit processors
        writeln("    X86");
    version(X86_64) // Intel and AMD 64-bit processors
        writeln("    X86_64");
    version(ARM) // The ARM architecture (32-bit) (AArch32 et al)
        writeln("    ARM");
    version(ARM_Thumb) // ARM in any Thumb mode
        writeln("    ARM_Thumb");
    version(ARM_SoftFloat) // The ARM soft floating point ABI
        writeln("    ARM_SoftFloat");
    version(ARM_SoftFP) // The ARM softfp floating point ABI
        writeln("    ARM_SoftFP");
    version(ARM_HardFloat) // The ARM hardfp floating point ABI
        writeln("    ARM_HardFloat");
    version(AArch64) // The Advanced RISC Machine architecture (64-bit)
        writeln("    AArch64");
    version(AsmJS) // The asm.js intermediate programming language
        writeln("    AsmJS");
    version(AVR) // 8-bit Atmel AVR Microcontrollers
        writeln("    AVR");
    version(Epiphany) // The Epiphany architecture
        writeln("    Epiphany");
    version(PPC) // The PowerPC architecture, 32-bit
        writeln("    PPC");
    version(PPC_SoftFloat) // The PowerPC soft float ABI
        writeln("    PPC_SoftFloat");
    version(PPC_HardFloat) // The PowerPC hard float ABI
        writeln("    PPC_HardFloat");
    version(PPC64) // The PowerPC architecture, 64-bit
        writeln("    PPC64");
    version(IA64) // The Itanium architecture (64-bit)
        writeln("    IA64");
    version(MIPS32) // The MIPS architecture, 32-bit
        writeln("    MIPS32");
    version(MIPS64) // The MIPS architecture, 64-bit
        writeln("    MIPS64");
    version(MIPS_O32) // The MIPS O32 ABI
        writeln("    MIPS_O32");
    version(MIPS_N32) // The MIPS N32 ABI
        writeln("    MIPS_N32");
    version(MIPS_O64) // The MIPS O64 ABI
        writeln("    MIPS_O64");
    version(MIPS_N64) // The MIPS N64 ABI
        writeln("    MIPS_N64");
    version(MIPS_EABI) // The MIPS EABI
        writeln("    MIPS_EABI");
    version(MIPS_SoftFloat) // The MIPS soft-float ABI
        writeln("    MIPS_SoftFloat");
    version(MIPS_HardFloat) // The MIPS hard-float ABI
        writeln("    MIPS_HardFloat");
    version(MSP430) // The MSP430 architecture
        writeln("    MSP430");
    version(NVPTX) // The Nvidia Parallel Thread Execution (PTX) architecture, 32-bit
        writeln("    NVPTX");
    version(NVPTX64) // The Nvidia Parallel Thread Execution (PTX) architecture, 64-bit
        writeln("    NVPTX64");
    version(RISCV32) // The RISC-V architecture, 32-bit
        writeln("    RISCV32");
    version(RISCV64) // The RISC-V architecture, 64-bit
        writeln("    RISCV64");
    version(SPARC) // The SPARC architecture, 32-bit
        writeln("    SPARC");
    version(SPARC_V8Plus) // The SPARC v8+ ABI
        writeln("    SPARC_V8Plus");
    version(SPARC_SoftFloat) // The SPARC soft float ABI
        writeln("    SPARC_SoftFloat");
    version(SPARC_HardFloat) // The SPARC hard float ABI
        writeln("    SPARC_HardFloat");
    version(SPARC64) // The SPARC architecture, 64-bit
        writeln("    SPARC64");
    version(S390) // The System/390 architecture, 32-bit
        writeln("    S390");
    version(SystemZ) // The System Z architecture, 64-bit
        writeln("    SystemZ");
    version(HPPA) // The HP PA-RISC architecture, 32-bit
        writeln("    HPPA");
    version(HPPA64) // The HP PA-RISC architecture, 64-bit
        writeln("    HPPA64");
    version(SH) // The SuperH architecture, 32-bit
        writeln("    SH");
    version(WebAssembly) // The WebAssembly virtual ISA (instruction set architecture), 32-bit
        writeln("    WebAssembly");
    version(WASI) // The WebAssembly System Interface
        writeln("    WASI");
    version(Alpha) // The Alpha architecture
        writeln("    Alpha");
    version(Alpha_SoftFloat) // The Alpha soft float ABI
        writeln("    Alpha_SoftFloat");
    version(Alpha_HardFloat) // The Alpha hard float ABI
        writeln("    Alpha_HardFloat");
    version(LittleEndian) // Byte order, least significant first
        writeln("    LittleEndian");
    version(BigEndian) // Byte order, most significant first
        writeln("    BigEndian");
    version(ELFv1) // The Executable and Linkable Format v1
        writeln("    ELFv1");
    version(ELFv2) // The Executable and Linkable Format v2
        writeln("    ELFv2");
    version(D_BetterC) // D as Better C code (command line switch -betterC) is being generated
        writeln("    D_BetterC");
    version(D_Exceptions) // Exception handling is supported. Evaluates to false when compiling with command line switch -betterC
        writeln("    D_Exceptions");
    version(D_ModuleInfo) // ModuleInfo is supported. Evaluates to false when compiling with command line switch -betterC
        writeln("    D_ModuleInfo");
    version(D_TypeInfo) // Runtime type information (a.k.a TypeInfo) is supported. Evaluates to false when compiling with command line switch -betterC
        writeln("    D_TypeInfo");
    version(D_Coverage) // Code coverage analysis instrumentation (command line switch -cov) is being generated
        writeln("    D_Coverage");
    version(D_Ddoc) // Ddoc documentation (command line switch -D) is being generated
        writeln("    D_Ddoc");
    version(D_InlineAsm_X86) // Inline assembler for X86 is implemented
        writeln("    D_InlineAsm_X86");
    version(D_InlineAsm_X86_64) // Inline assembler for X86-64 is implemented
        writeln("    D_InlineAsm_X86_64");
    version(D_LP64) // Pointers are 64 bits (command line switch -m64). (Do not confuse this with C's LP64 model)
        writeln("    D_LP64");
    version(D_X32) // Pointers are 32 bits, but words are still 64 bits (x32 ABI) (This can be defined in parallel to X86_64)
        writeln("    D_X32");
    version(D_HardFloat) // The target hardware has a floating-point unit
        writeln("    D_HardFloat");
    version(D_SoftFloat) // The target hardware does not have a floating-point unit
        writeln("    D_SoftFloat");
    version(D_PIC) // Position Independent Code (command line switch -fPIC) is being generated
        writeln("    D_PIC");
    version(D_PIE) // Position Independent Executable (command line switch -fPIE) is being generated
        writeln("    D_PIE");
    version(D_SIMD) // Vector extensions (via __simd) are supported
        writeln("    D_SIMD");
    version(D_AVX) // AVX Vector instructions are supported
        writeln("    D_AVX");
    version(D_AVX2) // AVX2 Vector instructions are supported
        writeln("    D_AVX2");
    version(D_Version2) // This is a D version 2 compiler
        writeln("    D_Version2");
    version(D_NoBoundsChecks) // Array bounds checks are disabled (command line switch -boundscheck=off)
        writeln("    D_NoBoundsChecks");
    version(D_ObjectiveC) // The target supports interfacing with Objective-C
        writeln("    D_ObjectiveC");
    version(Core) // Defined when building the standard runtime
        writeln("    Core");
    version(Std) // Defined when building the standard library
        writeln("    Std");
    version(unittest) // Unit tests are enabled (command line switch -unittest)
        writeln("    unittest");
    version(assert) // Checks are being emitted for AssertExpressions
        writeln("    assert");
    version(none) // Never defined; used to just disable a section of code
        writeln("    none");
    version(all) // Always defined; used as the opposite of none
        writeln("    all");

    writeln("} were detected.\n");
    writeln("Press the enter/return key to continue.");
    readln();
}