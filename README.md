# Test Tags

This apps purpose is simply to test what version tags
will build into the app on a given system. It can be
used for finding out what options are available when
used on a target audience's machine, or simply to
debug when certain code is (but shouldnt) or isnt
(but should) being activated.

# Example Output

Running bin/test-tags.exe 
Version Tags {
    DigitalMars
    Windows    
    Win64
    CRuntime_Microsoft
    CppRuntime_Microsoft
    X86_64
    LittleEndian
    D_Exceptions
    D_ModuleInfo
    D_TypeInfo
    D_InlineAsm_X86_64
    D_LP64
    D_HardFloat
    D_SIMD
    D_Version2
    assert
    all
} were detected.